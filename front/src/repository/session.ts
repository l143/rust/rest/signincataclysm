import { Fetch } from "./fetch";

export class SessionRepo extends Fetch {
  constructor(base: string) {
    super(base)
  }

  tryLogin = (email: string, password: string) => {
    return this.post(`/api/auth`, {email, password})
  };

}