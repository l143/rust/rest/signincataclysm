#[macro_use]
extern crate diesel;

pub mod modules;
pub mod infra;
pub mod schema;
pub mod errors;