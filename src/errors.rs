use std::result;
use serde::Serialize;
use thiserror::Error;

#[derive(Error, Debug)]
pub enum Error {
  #[error("Bad data: {0}")]
  BadData(String),
  #[error("Not found")]
  NotFound,
  #[error("Not authorized")]
  NotAuthorized,
  #[error("DB pool connection error")]
  InternalDBPool,
  #[error("Error trying to hash the password {0}")]
  InternalHashError(String),
  #[error("IO database error {0}")]
  InternalIoError(String),
  #[error("Jwt error {0}")]
  InternalJWTError(String),
}

pub type Result<T> = result::Result<T, Error>;


#[derive(Serialize)]
pub struct ApiError {
  error: String
}
impl ApiError {
  pub fn new(error: String) -> Self {
    ApiError { error }
  }
}