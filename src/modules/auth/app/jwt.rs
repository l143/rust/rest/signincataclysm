use chrono::Utc;
use crate::errors::{Error, Result};
use jsonwebtoken::{encode, decode, Header, Algorithm, EncodingKey, DecodingKey, Validation };


use crate::modules::auth::{dtos::Payload};


pub enum TokenType {
  Refresh,
  Access
}

pub struct JwtManager {
  secret: String,
}

impl JwtManager {
  pub fn new(secret: String) -> Self {
    return JwtManager { secret: secret }
  }

  pub fn create_jwt(&self, uid: String, token_type: TokenType) -> Result<String> {

    let duration = match token_type {
      TokenType::Refresh => {
        chrono::Duration::minutes(6)
      },
      TokenType::Access => {
        chrono::Duration::minutes(5)
      }
    };

    let expiration = Utc::now()
      .checked_add_signed(duration)
      .expect("valid timestamp")
      .timestamp();
  
    let claims = Payload {
      sub: uid,
      exp: expiration as usize
    };
  
    let header = Header::new(Algorithm::HS512);
  
    match encode(&header, &claims, &EncodingKey::from_secret(self.secret.as_bytes())) {
      Ok(t) => { Ok(t) },
      Err(e) => { Err(Error::InternalJWTError(e.to_string())) }
    }
  }

  pub fn validate_jwt(&self, jwt: String) -> Result<Payload> {
    let decoded = match decode::<Payload>(
      &jwt,
      &DecodingKey::from_secret(self.secret.as_bytes()),
      &Validation::new(Algorithm::HS512)
    ) {
      Ok(d) => {d},
      Err(e) => {
        return Err(Error::InternalJWTError(e.to_string()))
      }
    };
  
    Ok(decoded.claims)
  }
}
