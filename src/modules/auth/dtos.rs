use serde::{Deserialize, Serialize};

#[derive(Deserialize, Serialize, Debug)]
pub struct Payload {
  pub sub: String,
  pub exp: usize
}
