use log::{info};
use serde::{Serialize, Deserialize};
use cataclysm::{Branch, Shared, session::Session, http::{Method, Response, Json}};

use crate::infra::state::State;
use crate::errors::{Error, ApiError};
use crate::modules::users::app::UserService;

use super::super::app::jwt::{JwtManager, TokenType};

#[derive(Deserialize)]
struct Credentials { 
  pub email: String,
  pub password: String
}

#[derive(Serialize)]
struct SuccesResponse {
  access_token: String
}

async fn login(payload: Json<Credentials>, shared: Shared<State>, mut session: Session) -> Response {
  let data = payload.into_inner();
  let svc = UserService::new(shared.user_repo.clone());

  info!("Login attempt for {}", data.email);

  match svc.authenticate(data.email, data.password) {
    Ok(ok_user) => {

      let jwt = JwtManager::new("someajksf".to_string());
      let access_token = match jwt.create_jwt(ok_user.id.clone(), TokenType::Access) {
        Ok(r) => {r},
        Err(_) => { 
          return Response::internal_server_error()
        }
      };
      let refresh_token = match jwt.create_jwt(ok_user.id, TokenType::Access) {
        Ok(r) => {r},
        Err(_) => { 
          return Response::internal_server_error()
        }
      };

      session.set("refresh", refresh_token);
      session.apply( Response::ok()
        .header("Content-type", "application/json")
        .body(serde_json::to_string(&SuccesResponse{access_token}).unwrap())
      )
    },
    Err(e) => {
      match e {
        Error::NotFound => {
          return Response::not_found()
        },
        Error::NotAuthorized => {
          let res_error = ApiError::new("bad_credentials".to_string());
          return Response::forbidden()
            .header("Content-type", "application/json")
            .body(serde_json::to_string(&res_error).unwrap())
        },
        _ => {
          println!("{}", e);
          return Response::internal_server_error()
        }
      }
    }
  }
}

pub fn get_handlers () -> Branch<State> {
  Branch::new("/auth")
    .with(Method::Post.to(login))
}