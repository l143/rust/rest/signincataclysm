use crate::schema::users;

#[derive(Insertable)]
#[table_name="users"]
pub struct NewUser {
  pub id: String,
  pub email: String,
  pub password: String
}

#[derive(Queryable, Debug, Clone)]
pub struct User {
  pub id: String,
  pub email: String,
  pub password: String
}