use crate::errors::Result;
use super::models::{User, NewUser};

pub trait UserRepository {
  fn get_by_email(&self, email: String) -> Result<User>;
  fn save(&self, user: NewUser) -> Result<User>;
}