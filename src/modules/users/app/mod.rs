use std::sync:: Arc;

use uuid::Uuid;
use argon2::{
  password_hash::{
    rand_core::OsRng,
    PasswordHasher, SaltString,
    PasswordHash, PasswordVerifier
  },
  Argon2
};

use super::domain::UserRepository;
use super::models::{NewUser, User};
use super::dtos::create_user::CreateUserDto;

use crate::errors::{Result, Error};

pub struct UserService {
  repo: Arc<dyn Sync + Send + UserRepository>
}

impl UserService {
  pub fn new(repo: Arc<dyn Sync +  Send + UserRepository>) -> Self {
    UserService{ repo }
  }

  pub fn authenticate(&self, email: String, password: String) -> Result<User> {
    let user = self.repo.get_by_email(email)?;
    let parsed_hash = match PasswordHash::new(&user.password) {
      Ok(p) => {p},
      Err(e) => {
        return Err(Error::InternalHashError(e.to_string()))
      }
    };

    let is_ok = Argon2::default().verify_password(password.as_bytes(), &parsed_hash).is_ok();

    if !is_ok {
      return Err(Error::NotAuthorized)
    }

    Ok(user)

  }

  pub fn create(&self, email: String, password: String) -> Result<User> {
    //  Validate input data 
    CreateUserDto::new(email.clone(), password.clone())?;
    // Hash password
    let salt = SaltString::generate(&mut OsRng);
    let argon2 = Argon2::default();
    let password_hash = match argon2.hash_password(password.clone().as_bytes(), &salt) {
      Ok(p) => {p},
      Err(e) => {
        return Err(Error::InternalHashError(e.to_string()))
      }
    };

    let new_user = NewUser{id: Uuid::new_v4().to_string(), email, password: password_hash.to_string()};
    let saved_user = self.repo.save(new_user)?;
    Ok(saved_user)
  }
}