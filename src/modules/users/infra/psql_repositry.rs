use r2d2::Pool;
use diesel::{PgConnection, RunQueryDsl};
use diesel::r2d2::ConnectionManager;
use diesel::result::Error as DError;

use diesel::prelude::*;
use crate::schema::users as users_schema;
use crate::schema::users::dsl::email;
use crate::errors::{Result, Error};
use super::super::domain::UserRepository;
use super::super::models::{NewUser, User};

pub struct PsqlRepository {
  pub pool: Pool<ConnectionManager<PgConnection>>
}

impl UserRepository for PsqlRepository {
  fn get_by_email(&self, eemail: String) -> Result<User> {
    let conn = self.pool.get().unwrap();
    
    match users_schema::table
      .filter(email.eq(eemail))
      .limit(1)
      .load::<User>(&conn) {
      Ok( users ) => {
        if users.len() > 0 {
          return Ok(users[0].clone())
        }
        return Err(Error::NotFound)
      },
      Err(e) => {
        match e {
          DError::DatabaseError(_,_) => {
            return Err(Error::BadData(e.to_string()))
          },
          _ => {
            return Err(Error::InternalIoError(e.to_string()))
          }
        }
      }
    }
  }

  fn save(&self, new_user: NewUser) -> Result<User> {
    let conn = self.pool.get().unwrap();
    let saved_user = match diesel::insert_into(users_schema::table)
      .values(new_user)
      .get_result::<User>(&conn) {
        Ok(r) => {r},
        Err(e) => {
          match e {
            DError::DatabaseError(_,_) => {
              return Err(Error::BadData(e.to_string()))
            },
            _ => {
              return Err(Error::InternalIoError(e.to_string()))
            }
          }
        }
      };
    Ok(saved_user)
  }
}