use serde::{Serialize, Deserialize};
use cataclysm::{Branch, Shared, http::{Method, Response, Json}};

use crate::errors::{Error, ApiError};
use crate::infra::state::State;
use super::super::app::UserService;

#[derive(Deserialize, Debug)]
struct CreateUserBody {
  email: String,
  password: String
}

#[derive(Serialize)]
struct CreatedUser {
  id: String,
  email: String
}

async fn create_handler(payload: Json<CreateUserBody>, shared: Shared<State>) -> Response {
  let data = payload.into_inner();
  let svc = UserService::new(shared.user_repo.clone());

  match svc.create(data.email, data.password) {
    Ok(user) => {
      let response = CreatedUser {
        id: user.id,
        email: user.email
      };
      Response::created()
        .header("Content-Type", "application/json")
        .body(serde_json::to_string(&response).unwrap())
    },
    Err(e) => {
      match e {
          Error::BadData(ie) => {
            let error_res = ApiError::new(ie.to_string());
            Response::bad_request()
              .header("Content-Type", "application/json")
              .body(serde_json::to_string(&error_res).unwrap())
          },
          _ => {
            Response::internal_server_error().body(e.to_string())
          }
      }
    }
  }
}


pub fn get_handlers() -> Branch<State> {
  Branch::<State>::new("/user")
    .with(Method::Post.to(create_handler))
}