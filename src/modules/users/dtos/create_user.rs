use validator::{Validate};
use serde::{Deserialize, Serialize};

use crate::errors::{Error::{BadData}, Result};

#[derive(Deserialize, Serialize, Validate)]
pub struct CreateUserDto {
  #[validate(email)]
  pub email: String,
  #[validate(length(min = 12))]
  pub password: String
}

impl CreateUserDto {
  pub fn new(email: String, password: String) -> Result<CreateUserDto> {
    let dto = CreateUserDto {
      email,
      password
    };

    if let Err(e) = dto.validate() {
      return Err(BadData(e.to_string()))
    }

    return Ok(dto)
  }
}