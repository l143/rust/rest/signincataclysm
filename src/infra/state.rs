use std::sync::Arc;
// use r2d2::Pool;
// use diesel::pg::PgConnection;
// use diesel::r2d2::ConnectionManager;

use crate::modules::users::domain::UserRepository;

pub struct State {
  pub user_repo: Arc<dyn Sync + Send + UserRepository>
}