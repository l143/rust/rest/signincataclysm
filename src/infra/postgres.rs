use core::time::Duration;
use crate::errors::{Result, Error};

use r2d2::Pool;
use diesel::pg::PgConnection;
use diesel::r2d2::ConnectionManager;


pub fn create_pool(uri: String) -> Result<Pool<ConnectionManager<PgConnection>>> {
  let manager = ConnectionManager::<PgConnection>::new(uri);
  match r2d2::Pool::builder()
    .max_size(15)
    .idle_timeout(Some(Duration::new(10, 0)))
    .build(manager) {
      Ok(p) => {
        Ok(p)
      },
      Err(_) => {
        Err(Error::InternalDBPool)
      }
    }
}