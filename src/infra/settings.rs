use std::env;
use dotenv::dotenv;

pub struct Server {
  pub host: String,
  pub port: String
}

pub struct Settings {
  pub db_host: String,
  pub server: Server
}

impl Settings {
  pub fn new() -> Self {
    dotenv().ok();
    Settings { 
      db_host: env::var("DATABASE_URL").expect("Missing db host"), 
      server: Server { 
        host: env::var("HOST").expect("Missing server host"),
        port: env::var("PORT").expect("Missing server port") 
      }
    }
  }
}