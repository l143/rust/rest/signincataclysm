use std::sync::Arc;
use log::{info};
use cataclysm::{Branch, Server, CorsBuilder, session::{CookieSession, SameSite}};

use signin::infra::settings::Settings;

use signin::infra::{state::State, postgres::create_pool};
use signin::modules::users::{
  infra::handlers::get_handlers as get_user_branch,
  infra::psql_repositry::PsqlRepository
};

use signin::modules::auth::infra::handler::get_handlers as get_auth_handlers;


#[tokio::main]
async fn main() {
  let settings = Settings::new();
  
  env_logger::init();

  let pool = match create_pool(settings.db_host) {
    Ok(p) => {p},
    Err(e) => {
      panic!("{}", e);
    }
  };

  let user_repo = Arc::new(PsqlRepository {
    pool: pool.clone()
  });

  let user_branch = get_user_branch();
  let auth_branch = get_auth_handlers();
  let main_branch = Branch::new("/api").nest(
    user_branch
      .merge(auth_branch)
  );

  let cors_config = CorsBuilder::new()
    .origin("http://localhost:5173")
    .max_age(600)
    .build().unwrap();

  let server = Server::builder(main_branch)
    .share(State { user_repo })
    .session_creator(CookieSession::new().same_site(SameSite::None))
    .cors(cors_config)
    .build()
    .unwrap();

  let address = format!("{}:{}", settings.server.host, settings.server.port);
  info!("Starting server in {}", address.clone());
  server.run(address.clone()).await.expect("Bad server init");
}